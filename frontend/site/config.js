  /*
* All configurations for frontend must be here, 
* do not modify any setup from grunt's task
* Remenber this skeleton support integration with most framework
*/
var config = {
  project_name: 'Skelsus',
  template_ext: function () {
    var ext;
    if (this.env === 'prod') {
      return '.html';
    }
    return '.html';
  },
  static_uri: function () {
    /* Método para cargar los archivos státicos CSS, JS, Images */
    if (this.env === "prod") {
      return '/static';
    }
    return '/static';    
  },
  deploy_routes : function () {
    var routes = {},
        base,
        static_path;
    if (this.env == 'dev') {
      base = 'build';
      static_path =  base + '/static';
      routes = {
        base : base,
        templates: base + '/templates',
        static: static_path,
        styles: static_path + '/styles',
        images: static_path + '/images',
        fonts: static_path + '/fonts',
        scripts: static_path + '/scripts',
        sprites: static_path + '/sprites'
      }      
    } else {
      base = '../../public/';
      static_path =  base;
      routes = {
        base: '',
        templates: base + '',
        static: static_path,
        styles: static_path + 'styles/',
        images: static_path + 'images/',
        fonts: static_path + 'fonts/',
        scripts: static_path + 'scripts/',
        sprites: static_path + 'sprites/'
      }
    }
    return routes;
  },
  template_uri: function(name) {
    return this.urls[name].get('uri');
  },
  reverse_url: function(name) {
    if(this.env == 'prod'){
      return "{% url '" + this.urls[name].get('url') + "' %}";
    }
    return this.urls[name].get('template');
  },
  static_url: function(url) {
    if (this.env == 'prod') {
      return "./" + url;
    }
    return this.static_uri() + '/' + url;
  },
  setEnv : function (env) {
    this.env = env;
  },
  getEnv: function () {
    return this.env;
  },
  urls: {},
  set_url: function(name, obj) {
    this.urls[name] = new this.Url(obj);
  },
  Url: function(obj) {
    this.obj = obj;
    this.get = function(name) {
      return this.obj[name];
    };
    return this;
  }
};

config.set_url('validator', {
  url: 'validator',
  template : 'validator.html'
});

config.set_url('pre_home', {
  url: 'pre_home',
  template : 'pre-home.html'
});

config.set_url('home', {
  url: 'home',
  template : 'home.html'
});

config.set_url('register', {
  url: 'register',
  template : 'register.html'
});

config.set_url('thanks', {
  url: 'thanks',
  template : 'thanks.html'
});

config.set_url('tyc', {
  url: 'tyc',
  template : 'terms.html'
});

config.set_url('provinces', {
  url: 'provinces',
  template : ''
});

config.set_url('districts', {
  url: 'districts',
  template : ''
});

config.set_url('complete_register', {
  url: 'complete_register',
  template : 'register.html'
});

config.set_url('connect_facebook', {
  url: 'connect_facebook',
  template : 'home.html'
});

config.set_url('connect_twitter', {
  url: 'connect_twitter',
  template : 'home.html'
});

config.set_url('validator', {
  url: 'validator',
  template : 'validator.html'
});


module.exports = config;