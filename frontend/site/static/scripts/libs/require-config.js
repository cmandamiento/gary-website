require.config({
  baseUrl:'/static/scripts',
  paths: {
    'fb': 'modules/fb',
    'tw': 'modules/tw',
    'async': 'libs/require.async',
    'autotab' : 'libs/jquery.autotab',
    'alphanumeric': 'libs/jquery.alphanumeric',
    'register-form': 'modules/register-form',
    'easyModal': 'modules/easyModal',
    'floated-label': 'libs/floated.label',
    'custom-input': 'modules/custom-input',
    'validate': 'libs/jquery.validate/jquery.validate',
    'jquery': 'libs/jquery-1.11.0',
    'validate-ext':'libs/jquery.validate/additional-methods',
    'datepicker':'libs/jquery-ui-1.10.4.datepicker',
    'jquery-ui': 'libs/jquery-ui-1.9.2.custom'
  },
  shim: {
    'alphanumeric':[
      'jquery'
    ],
    'validate' : [
      'jquery'
    ],
    'validate-ext':[
      'validate'
    ],
    'floated-label' : [
      'jquery'
    ],
    'register-form' : [
      'validate',
      'alphanumeric'
    ],
    'custom-input' : [
      'jquery'
    ],
    'autotab' : [
      'jquery'
    ],
    'search' : [
      'autogrow',
      'jquery-ui'
    ],
    'easyModal': [
      'jquery'
    ]
  }
});