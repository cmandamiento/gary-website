require([
  'fb',
  'tw',
  'register-form'
  ], function (fb, tw, validate) {

    var $doc = $(document);

    function init() {
      fb.parseEls();
      tw.parseEls();
      validate.setupRegisterForm({
        form: '.Register-form',
        tracking:{
          category:'',
          action:'',
          label:''
        },
        isAjax: false,
      });
    }

    $doc.ready(init);
});