define([
  'datepicker',
  'validate-ext'
], function () {
  var EasyModal,
      defaults;

  defaults = {
    'padding': 0,
    'width': 450,
    'height': 320,
    'isFixed': false,
    'content': "This is a content example",
    'onClose': null
  };

  function EasyModal(options) {      
      this.options = $.extend(defaults,  options);      
      this.init();
  }
    
  EasyModal.prototype = {
    constructor : EasyModal,
    init: function() {
      $('body').prepend(
        $('<div class="overlay"></div>'),
        $('<div class="modal"></div>')
      );
      this.$modal = $('.modal');
      this.$modal.append(this.options.content);
      this.$overlay = $('.overlay');
      this._setupEventCloseModal();
    },
    setContent: function (content) {
      this.$modal.html(content);
    },
    getContent : function () {
      return this.$modal.html();
    },
    _setupEventCloseModal: function() {
      var $body = $('body');

      this.$modal.find('.modal-close').on('click',  { obj: this }, this.close);
      this.$overlay.on('click', { obj: this }, this.close);

      $('document').on('keydown', function() {
        if (e.keyCode == 27) {
          this.close();
        }
      });
    },
    close: function(e) {
      var onClose,
          data,
          self;

      data = e.data;
      self = data.obj;

      onClose = self.options.onClose;
      self.$overlay.fadeOut(200);
      self.$modal.fadeOut(200);
      self.$modal.removeClass('is-show');
      if (onClose) {
        callback();
      }
    },
    show: function() {
      var $body,
          $calculatedPositionLeft,
          $calculatedPositionTop,
          $topScrollbar;
      
      this.$overlay.show();
      $body = $('body');
      calculatedPositionLeft = parseInt(($body.width() - this.options.width) / 2);
      calculatedPositionTop = parseInt(($body.height() - this.options.height) / 2);
      topScrollbar = $(document).scrollTop();

      this.$modal.removeClass('is-show');
      if (!this.options.isFixed) {
        $body.addClass('u-noScroll');
        this.$modal.css({
          'top': calculatedPositionTop + 'px',
          "padding": this.options.padding + "px",
          "width": this.options.width + "px",
          "height": this.options.height + "px",
          "left": calculatedPositionLeft + 'px'
        });
      } else {
        this.$modal.css({
          'top': (topScrollbar + 50) + 'px',
          "padding": this.options.padding + "px",
          "width": this.options.width + "px",
          "height": this.options.height + "px",
          "left": calculatedPositionLeft + 'px'
        });
      }
      this.$modal.fadeIn(600);
    }
  }
  return EasyModal;
});