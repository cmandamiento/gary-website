module.exports = function(grunt) {
  try {
    // This var config is global in all task files
    config = require('./config.js');
    if (grunt.option('prod')) {
      config.setEnv('prod');
    } else {
      config.setEnv('dev');
    }
  } catch (e) {

    config = {};
  }
  grunt.config.set('config', config);
  grunt.loadTasks('./tasks');
};