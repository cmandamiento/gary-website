  module.exports = function(grunt) {

  var dest = config.deploy_routes().scripts;

  grunt.loadNpmTasks('grunt-contrib-requirejs');

  grunt.config.set('requirejs', {
    deploy: {
      options: {
        appDir:'static/scripts',
        mainConfigFile: "static/scripts/libs/require-config.js",
        baseUrl: '.',
        dir: dest,
        preserveLicenseComments: false,
        modules: [
        ]
      }
    }
  });

  grunt.registerTask('scripts',['requirejs:deploy','notify:scripts']);
};