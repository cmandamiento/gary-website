module.exports = function(grunt) {

  grunt.loadNpmTasks('grunt-notify');

  grunt.config.set('notify', {
    watch: {
      options: {
        title: 'Watch',
        message: 'Updated files are compiled'
      }
    },
    connect: {
      options: {
        title: 'Conect',
        message: 'Static server is ready'
      }
    },
    sprites: {
      options: {
        title: 'Glue',
        message: 'Sprites generated'
      }
    },
    templates: {
      options: {
        title: 'Templates',
        message: 'Jade compiled'
      }
    },    
    copy_assets: {
      options: {
        title: 'Copy',
        message: 'Images, sprites and fonts copied'
      }
    },
    scripts: {
      options: {
        title: 'Requirejs',
        message: 'Requirejs modules compiled'
      }
    },
    styles: {
      options: {
        title: 'Css',
        message: 'Stylus compiled',
      }
    }
  });
};