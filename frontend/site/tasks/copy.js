module.exports = function(grunt) {
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.config.set('copy', {
    images: {
      files: [{
        expand: true,
        cwd: 'static/images',
        src: ['*', '**/*'],
        dest: config.deploy_routes().images
      }]
    },
    fonts: {
      files: [{
        expand: true,
        cwd: 'static/fonts',
        src: ['*', '**/*'],
        dest: config.deploy_routes().fonts
      }]
    }
  });

  grunt.registerTask('copy_assets', '', function() {
    grunt.task.run([
      'copy',
      'notify:copy_assets'
    ]);
  });

}