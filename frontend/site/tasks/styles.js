module.exports = function(grunt) {
  
  var config = grunt.config.get('config'),
      dest   = config.deploy_routes().styles;

  grunt.loadNpmTasks('grunt-contrib-stylus');

  grunt.config.set('stylus', {
    compile: {
      options: {
        compress: true
      },
      files: [
        {
          expand: true,
          cwd: 'static/styles',
          src: [
            '**.styl'
          ],
          dest: dest,
          ext: '.css'
        }
      ]
    }
  });
  grunt.registerTask('styles', 'Compiling Templates', function () {
    if (grunt.option('dev')) {
      grunt.task.run(['stylus:compile']);
    } else {
      grunt.task.run(['stylus:compile']);      
    }
  });
};
