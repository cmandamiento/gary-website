var grunt,
  path = require('path'),
  ext = config.template_ext(),
  local_config = {
    watch: {
      template: {
        files: ['templates/**.jade', 'templates/**/*.jade', 'static/**/**.styl'],
        options: {
          spawn: false,
          livereload: true,
          interrupt: true
        }
      }
    }
  };

/**
 * onWatch, se ejecuta con cada cambio de los archivos revisados por el
 * watcher
 * @param action string tipo de cambio
 * @param filepath string Ruta de archivo modificado
 */
function onWatch(action, filepath) {

  var ext = filepath.split('.');
  ext = ext[ext.length - 1];

  if (ext === 'jade') {
    cJade(grunt, filepath, local_config.jade, local_config.htmlmin);
  } else if (ext === 'styl') {
    cStylus(grunt, filepath, local_config.stylus, local_config.cssmin);
  }
}

/**
 * Compilar Stylus, modifica los valores de la tarea stylus.js para compilar
 * solo los archivos que son modificados
 * @param grunt object instancia de grunt
 * @param filepath string Ruta de archivo modificado
 * @param default_config array Configuracion inicial de los archivos
 */
function cStylus(grunt, filepath, default_config, default_config_min) {
  console.log(filepath);
  filepath = filepath.split('static' + path.sep + 'styles' + path.sep)[1];

  if (
    filepath.indexOf('modules' + path.sep) === -1 &&
    filepath.indexOf('sections' + path.sep) === -1 &&
    filepath.indexOf('libs' + path.sep) === -1
  ) {
    grunt.config.set('stylus.compile.files.0.src', [filepath]);
    grunt.config.set('cssmin.compile.files.0.src', [filepath]);

  } else {
    grunt.config.set('stylus.compile.files.0.src', default_config);
    grunt.config.set('cssmin.compile.files.0.src', default_config_min);
  }  

  grunt.task.run('styles');
}

/**
 * Compilar Jade, modifica los valores de la tarea jade.js para compilar
 * solo los archivos que son modificados
 * @param grunt object instancia de grunt
 * @param filepath string Ruta de archivo modificado
 * @param default_config array Configuracion inicial de los archivos
 */
function cJade(grunt, filepath, default_config, default_config_min) {
  // path.sep, para que funcione con back slash de windows
  filepath = filepath.split('templates' + path.sep)[1];
  console.log(filepath);
  if (/_(.*).jade/.test(filepath) === false) {
    filepath = filepath.split('sections' + path.sep)[1];
    grunt.config.set('jade.compile.files.0.src', [filepath]);
    grunt.config.set('htmlmin.compile.files.0.src', [filepath.replace('.jade', ext)]);
    console.log(filepath + ext)
  } else {
    grunt.config.set('jade.compile.files.0.src', default_config);
    grunt.config.set('htmlmin.compile.files.0.src', default_config_min);
  }
  
  grunt.task.run('templates');
}

module.exports = function(g) {
  // asigno valor a variable glogal
  grunt = g;
  // cargo libreria
  grunt.loadNpmTasks('grunt-contrib-watch');
  // copio configuracion sin referencias
  local_config.jade = grunt.config.get('jade.compile.files.0.src').slice(0) || [];
  local_config.htmlmin = grunt.config.get('htmlmin.compile.files.0.src').slice(0) || [];
  local_config.stylus = grunt.config.get('stylus.compile.files.0.src').slice(0) || [];
  local_config.cssmin = grunt.config.get('cssmin.compile.files.0.src').slice(0) || [];

  // Configuro watch
  grunt.config.set('watch', local_config.watch);


  // Configuro metodo de evento
  grunt.event.on('watch', onWatch);

};