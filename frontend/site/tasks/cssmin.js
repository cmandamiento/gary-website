module.exports = function(grunt) {

  var dest = config.deploy_routes().styles;

  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.config.set('cssmin', {
    compile: {
      files: [{
        expand: true,
        cwd: dest,
        src: ['*.css'],
        dest: dest,
        keepSpecialComments: false
      }]
    }
  });

}