module.exports = function(grunt) {
  
  var config  = grunt.config.get('config'),
      dest    = config.deploy_routes().sprites;

  grunt.loadNpmTasks('grunt-glue');

  grunt.registerTask('sprites', 'Compile sprites', function() {
    var items = grunt.file.expand({}, [
          'static/sprites/*', '!static/sprites/**.*'
        ]),
        i = items.length - 1,
        sprites = {};

    for (i; i > -1; --i) {
      var folder = items[i].split('/').pop(),
          namespace = folder === 'icons' ? 'icon':'sp';

      sprites['sprite' + i] = {
        src: [ items[i] ],
        options: '--css=' + dest +
          ' --namespace='+ namespace +
          ' --sprite-namespace= '+ namespace +
          ' --img=' + dest +
          ' --url=../sprites/ --margin=10'
      };
    }
    grunt.config.set('glue', sprites);

    grunt.config.set('stylus.glue', {
      options: {
        '-C': true
      },
      files: [
        {
          expand: true,
          cwd: dest,
          src: [
            '*.css'
          ],
          dest: 'static/styles/modules',
          ext: '.sprite.styl'
        }
      ]
    });

    grunt.task.run('glue');
    grunt.task.run('stylus:glue');
    grunt.task.run('notify:sprites');
  });
};
