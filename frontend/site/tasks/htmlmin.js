module.exports = function(grunt) {

  var dest,
      ext;

  grunt.loadNpmTasks('grunt-contrib-htmlmin');
  dest = config.deploy_routes().templates;
  ext = config.template_ext();
  grunt.config.set('htmlmin', {
    compile: {
      options: {                                 
        removeComments: true,
        collapseWhitespace: true,
        minifyJS: true
      },
      files: [
        {
          expand: true,
          cwd: dest,
          src:'**/*' + ext,
          dest: dest
        }
      ]
    }
  });
};

