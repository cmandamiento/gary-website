module.exports = function(grunt) {

  if (grunt.option('dev')) {
    grunt.registerTask('deploy', 'Deployando el proyecto en desarrollo', function() {
      grunt.task.run(['templates', 'styles', 'sprites']);
    });
  } else {
    grunt.registerTask('deploy', 'Deployando en proyecto en produccion', function() {
      grunt.task.run([
        'templates',
        'htmlmin',
        'styles',
        'sprites',
        'copy',
        'scripts'
      ]);
    });
  }

};